package com.prashantrahate.dualdb.dto.secondary;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity(name = "rating")
public class Rating implements Serializable {
	@JsonIgnore
	private static final transient long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	@JsonProperty("movie_id")
	@Column(name = "film_id")
	private long filmId;
	@JsonProperty(value = "customer_id")
	@Column(name = "customer_id")
	private long customerId;
	private short rate;
	private String review;
	@Column(name = "last_update")
	@Builder.Default
	private Date lastUpdated = new Date();
}
