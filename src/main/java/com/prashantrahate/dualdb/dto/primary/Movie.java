package com.prashantrahate.dualdb.dto.primary;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.prashantrahate.dualdb.dto.secondary.Rating;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity(name = "film")
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class Movie implements Serializable {
	@JsonIgnore
	private static final transient long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "film_film_id_seq")
	@Column(name = "film_id")
	private long id;
	private String title;
	private String description;
	@JsonProperty(value = "release_year")
	@Column(name = "release_year")
	private int releaseYear;
	@JsonProperty(value = "language_id")
	@Column(name = "language_id")
	private short languageId;
	@Column(name = "length")
	private int time;
	@JsonProperty(value = "last_update")
	@Column(name = "last_update")
	@Builder.Default
	private Date lastUpdated = new Date();
//	@Column(name = "fulltext")
//	private String fulltext;
//	@OneToMany(targetEntity = Rating.class) -- Mapping not possible because, while stating application framework search for 
	// mapped entity in same database
	@Transient
	private Collection<Rating> ratings;
}
