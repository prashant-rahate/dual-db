package com.prashantrahate.dualdb.dto;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResponseDetails implements Serializable {
	@JsonIgnore
	private static final transient long serialVersionUID = 1L;
	private Date timestamp;
	private long status;
	private String error;
	private String message;
}
