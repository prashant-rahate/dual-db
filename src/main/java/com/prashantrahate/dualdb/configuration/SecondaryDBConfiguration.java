package com.prashantrahate.dualdb.configuration;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(entityManagerFactoryRef = "secondaryEntityManagerFactory", transactionManagerRef = "secondaryTransactionManager", basePackages = {
		"com.prashantrahate.dualdb.repository.secondary", "com.prashantrahate.dualdb.dto.secondary" })
public class SecondaryDBConfiguration {

	@Bean(name = "secondaryDataSource")
	@ConfigurationProperties(prefix = "spring.secondary.datasource")
	public DataSource dataSource() {
		return DataSourceBuilder.create().build();
	}

	@Bean(name = "secondaryEntityManagerFactory")
	public LocalContainerEntityManagerFactoryBean entityManagerFactory(
			EntityManagerFactoryBuilder entityManagerFactoryBuilder,
			@Qualifier("secondaryDataSource") DataSource dataSource) {
		return entityManagerFactoryBuilder.dataSource(dataSource).packages(secondaryPackages()).build();
	}

	@Bean(name = "secondaryTransactionManager")
	public PlatformTransactionManager transactionManager(
			@Qualifier("secondaryEntityManagerFactory") EntityManagerFactory entityManagerFactory) {
		return new JpaTransactionManager(entityManagerFactory);
	}

	private String[] secondaryPackages() {
		List<String> packages = new ArrayList<>();
		packages.add("com.prashantrahate.dualdb.repository.secondary");
		packages.add("com.prashantrahate.dualdb.dto.secondary");
		return packages.toArray(new String[packages.size()]);
	}
}
