package com.prashantrahate.dualdb.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.prashantrahate.dualdb.dto.secondary.Rating;
import com.prashantrahate.dualdb.service.RatingService;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor(onConstructor = @__({ @Autowired }))
@RequestMapping("/movies/{movieId}/ratings")
public class RatingController {

	private final RatingService ratingService;

	@GetMapping
	public List<Rating> getRatingsByMovieId(@PathVariable("movieId") Long movieId) {
		return ratingService.findRatingByMovieId(movieId);
	}
	
	@PostMapping
	public Rating getRating(@PathVariable("movieId") Long movieId,
			@RequestBody Rating rating) {
		rating.setFilmId(movieId);
		return ratingService.persistRating(rating);
	}
}
