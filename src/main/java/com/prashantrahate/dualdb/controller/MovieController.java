package com.prashantrahate.dualdb.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.prashantrahate.dualdb.dto.primary.Movie;
import com.prashantrahate.dualdb.service.MovieService;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/movies")
@RequiredArgsConstructor(onConstructor =@__({@Autowired}))
public class MovieController {

	private final MovieService movieService;
	
	@GetMapping
	public List<Movie> getAllMovies(){
		return movieService.findAllMovies();
	}
	
	@GetMapping("/{movieId}")
	public Movie getMovieById(@PathVariable("movieId") Long movieId) {
		return movieService.findMovieById(movieId);
	}
	
	@PostMapping
	public Movie saveMovie(@RequestBody Movie movie) {
		return movieService.persistMovie(movie);
	}
	
	@PatchMapping("/{movieId}")
	public Movie updateMovie(@PathVariable("movieId") Long movieId,
			@RequestBody Movie movie) {
		return movieService.updateMovie(movieId, movie);
	}
	
	@DeleteMapping("/{movieId}")
	public void deleteMovie(@PathVariable("movieId") Long movieId) {
		movieService.deleteMovie(movieId);
	}
}
