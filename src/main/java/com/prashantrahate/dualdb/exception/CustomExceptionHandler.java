package com.prashantrahate.dualdb.exception;

import java.util.Date;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.prashantrahate.dualdb.dto.ResponseDetails;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestControllerAdvice
public class CustomExceptionHandler extends ResponseEntityExceptionHandler {
	
	@ExceptionHandler({DataIntegrityViolationException.class})
	@ResponseStatus(code = HttpStatus.PRECONDITION_FAILED)
	protected ResponseDetails pSQLExceptionHandler(DataIntegrityViolationException dataViolationException) {
		log.error(dataViolationException.getRootCause().getMessage());
		return ResponseDetails.builder()
				.timestamp(new Date())
				.status(HttpStatus.PRECONDITION_FAILED.value())
				.error(HttpStatus.PRECONDITION_FAILED.name())
				.message(dataViolationException.getRootCause().getMessage())
				.build();
	}
}
