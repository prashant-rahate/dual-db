package com.prashantrahate.dualdb.exception;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Component
@Getter
@Setter
@NoArgsConstructor
public class ValidationException extends RuntimeException {
	
	private static final transient long serialVersionUID = 1L;
	
	private HttpStatus status;
	private String message;
	private Exception exception;
	
	public ValidationException(String message) {
		super(message);
		this.status = HttpStatus.BAD_REQUEST;
		this.message = message;
	}
	
	public ValidationException(String message, Exception exception) {
		super(message, exception);
		this.status = HttpStatus.BAD_REQUEST;
		this.message = message;
		this.exception = exception;
	}
	
}
