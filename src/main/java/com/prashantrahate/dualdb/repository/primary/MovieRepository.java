package com.prashantrahate.dualdb.repository.primary;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.prashantrahate.dualdb.dto.primary.Movie;

@Repository
public interface MovieRepository extends JpaRepository<Movie, Long> {

}
