package com.prashantrahate.dualdb.repository.secondary;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.prashantrahate.dualdb.dto.secondary.Rating;

@Repository
public interface RateRepository extends JpaRepository<Rating, Long> {
	public List<Rating> findByFilmId(long movieId);
}
