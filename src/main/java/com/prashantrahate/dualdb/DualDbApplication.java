package com.prashantrahate.dualdb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DualDbApplication {

	public static void main(String[] args) {
		SpringApplication.run(DualDbApplication.class, args);
	}

}
