package com.prashantrahate.dualdb.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.prashantrahate.dualdb.dto.primary.Movie;
import com.prashantrahate.dualdb.repository.primary.MovieRepository;
import com.prashantrahate.dualdb.service.MovieService;
import com.prashantrahate.dualdb.service.RatingService;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor(onConstructor =@__({@Autowired}))
@Transactional(propagation = Propagation.REQUIRED)
public class MovieServiceImpl implements MovieService {
	private final MovieRepository movieRepository;
	private final RatingService ratingService;

	@Override
	public List<Movie> findAllMovies() {
		return movieRepository.findAll();
	}

	@Override
	public Movie findMovieById(long movieId) {
		Movie movie = movieRepository.getOne(movieId);
		movie.setRatings(ratingService.findRatingByMovieId(movie.getId()));
		return movie;
	}

	@Override
	public Movie persistMovie(Movie movie) {
		return movieRepository.save(movie);
	}

	@Override
	public Movie updateMovie(long movieId, Movie movie) {
		movie.setId(movieId);
		return movieRepository.save(movie);
	}

	@Override
	public void deleteMovie(long movieId) {
		movieRepository.deleteById(movieId);
	}
}
