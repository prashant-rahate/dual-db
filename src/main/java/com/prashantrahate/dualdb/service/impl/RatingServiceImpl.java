package com.prashantrahate.dualdb.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.prashantrahate.dualdb.dto.secondary.Rating;
import com.prashantrahate.dualdb.repository.secondary.RateRepository;
import com.prashantrahate.dualdb.service.RatingService;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor(onConstructor =@__({@Autowired}))
@Transactional(propagation = Propagation.REQUIRED)
public class RatingServiceImpl implements RatingService {

	private final RateRepository rateRepository;
	
	@Override
	public List<Rating> findRatingByMovieId(long movieId) {
		return rateRepository.findByFilmId(movieId);
	}

	@Override
	public Rating findRatingById(long ratingId) {
		return rateRepository.getOne(ratingId);
	}

	@Override
	public Rating persistRating(Rating rating) {
		return rateRepository.save(rating);
	}

	@Override
	public Rating updateRating(long ratingId, Rating rating) {
		rating.setId(ratingId);
		return rateRepository.save(rating);
	}

	@Override
	public void deleteRating(long ratingId) {
		rateRepository.deleteById(ratingId);
	}

}
