package com.prashantrahate.dualdb.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.prashantrahate.dualdb.dto.secondary.Rating;

@Service
public interface RatingService {
	public List<Rating> findRatingByMovieId(long movieId);
	public Rating findRatingById(long ratingId);
	public Rating persistRating(Rating rating);
	public Rating updateRating(long ratingId, Rating rating);
	public void deleteRating(long ratingId);
}
