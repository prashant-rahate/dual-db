package com.prashantrahate.dualdb.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.prashantrahate.dualdb.dto.primary.Movie;

@Service
public interface MovieService {
	public List<Movie> findAllMovies();
	public Movie findMovieById(long movieId);
	public Movie persistMovie(Movie movie);
	public Movie updateMovie(long movieId, Movie movie);
	public void deleteMovie(long movieId);
}
